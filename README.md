#TreasureQuest
Created by: Chris Bensler

A light and fun 2D action/rpg game for J2ME phones in the flavour of the classic game: Guantlet.

- Make your way through randomly generated mazes.
- Find all the keys to unlock the doors that block your way.
- Collect new weapons, armour and other items to help you on the quest to explore the dungeons and defeat your foes.

![Preview Image][img-preview]

[img-preview]: res/img/treasurequest.png