/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package game;

import main.Engine;

/**
 *
 * @author Chris
 */

public abstract class GameState {
  protected Engine engine;
  public GameState(Engine engine) {
    this.engine = engine;
  }
  /** invoked when this instance becomes active the current state (active) */
  protected void start() {};
  /** invoked when this instance */
  protected void stop() {};

  protected abstract void stateAction();
  protected abstract void stateUpdate();
}

