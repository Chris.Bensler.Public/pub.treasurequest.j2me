/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package main.maze;

import java.util.Random;
import javax.microedition.lcdui.Image;
import javax.microedition.lcdui.game.LayerManager;
import javax.microedition.lcdui.game.TiledLayer;

/**
 *
 * @author Chris
 */
public final class MazeRoom extends LayerManager {
 private static final int VOID_TILE = 1;
 private final MazeMap maze;
 private final Random rand = new Random();
 public final TiledLayer tlBase;   // non-interactive non-collision layer
 public final TiledLayer tlFringe; // non-interactive collision layer
 public final TiledLayer tlItems;  // interactive non-collision layer
 public final TiledLayer tlDoors; // interactive collision layer
 private final int tw,th;
 private int viewX, viewY;
 private int area;

  public MazeRoom(MazeMap maze, Image tset, Image tsGround, Image tsFringe, Image tsItems) {
   final int[] rgb;
   final int tx,ty, total_items;
   int frame;
   MazeKey key;
    tw = tset.getWidth()/MazeMap.tw;
    th = tw;
    viewX = 0;
    viewY = 0;
    maze.cellCurrent.visible = true;
    this.maze = maze;
    tlBase = new TiledLayer(tw,th,tsGround,16,16);
    tlFringe = new TiledLayer(tw,th,tsFringe,16,16);
    tlItems = new TiledLayer(tw,th,tsItems,16,16);
    tlDoors = new TiledLayer(tw,th,tsFringe,16,16);
    this.insert(tlBase,0);
    this.insert(tlFringe,0);
    this.insert(tlItems,0);
    this.insert(tlDoors,0);
    rand.setSeed(maze.SEED_VALUE+maze.XMAX*maze.cellCurrent.y+maze.cellCurrent.x);
    tx = maze.cellCurrent.framex;
    ty = maze.cellCurrent.framey;
    // generate room layer
    area = 0;
    rgb = new int[tw*th];
    tset.getRGB(rgb, 0, tw, tx*tw, ty*th, tw, th);
    for (int y=0; y < th; y++) {
      for (int x=0; x < tw; x++) {
        if (rgb[tw*y+x] != 0xFF000000) {
          area++;
          frame = 0;
          //  calculate outer fringe tiles
          if ((x == 0)      || (rgb[tw*y+x-1]    == 0xFF000000)) frame |= 1; // left
          if ((y == 0)      || (rgb[tw*(y-1)+x]  == 0xFF000000)) frame |= 2; // top
          if ((x == (tw-1)) || (rgb[tw*y+x+1]    == 0xFF000000)) frame |= 4; // right
          if ((y == (th-1)) || (rgb[tw*(y+1)+x]  == 0xFF000000)) frame |= 8; // bottom
          if (frame == 0) { // inside corner or full tile
            // check for inside corners
            if ((x != 0) && (y != 0)                && (rgb[tw*(y-1)+x-1] == 0xFF000000)) { // left/top
              frame = 12;
            } else if ((x != (tw-1)) && (y != (th-1)) && (rgb[tw*(y+1)+x+1] == 0xFF000000)) { // right/bottom
              frame = 3;
            } else if ((x != (tw-1)) && (y != 0)     && (rgb[tw*(y-1)+x+1] == 0xFF000000)) { // right/top
              frame = 9;
            } else if ((x != 0) && (y != (th -1))    && (rgb[tw*(y+1)+x-1] == 0xFF000000)) { // left/bottom
              frame = 6;
            }
            // random full tile
            tlBase.setCell(x, y, 2+rand.nextInt(5));
          // check for border edges
          } else if ((x == 0) || (x == (tw-1))) {
            if ((y == th/2-1) || (y == th/2)) {
              frame = 18;
              if (y == th/2) frame++;
              if (x == 0) {
                if (MazeMap.getLockDir(maze.cellCurrent, 1) == 0) frame += 4;
              } else {
                if (MazeMap.getLockDir(maze.cellCurrent, 4) == 0) frame += 4;
              }
              tlDoors.setCell(x,y,tlDoors.createAnimatedTile(frame));
              frame = VOID_TILE;
            }
          } else if ((y == 0) || (y == (th-1))) {
            if ((x == tw/2-1) || (x == tw/2)) {
              frame = 16;
              if (x == tw/2) frame++;
              if (y == 0) {
                if (MazeMap.getLockDir(maze.cellCurrent, 2) == 0) frame += 4;
              } else {
                if (MazeMap.getLockDir(maze.cellCurrent, 8) == 0) frame += 4;
              }
              tlDoors.setCell(x,y,tlDoors.createAnimatedTile(frame));
              frame = VOID_TILE;
            }
          }
          if (frame != 0) {
            tlFringe.setCell(x,y,frame);
          }
        }
      }
    }
    // check if any keys should be dropped in this room
    for (int i=0; i < maze.keys.size(); i++) {
      key = (MazeKey) maze.keys.elementAt(i);
      if (key.drop == maze.cellCurrent) {
        while (true) {
          if (newItem(rand.nextInt(tw),rand.nextInt(th),25+i)) break;
        }
      }
    }

    // generate random chests
    total_items = rand.nextInt((int) Math.floor(16.0*(((double) area)/(tw*th))));
    for (int i=0; i < total_items; i++) {
      while (true) {
        if (newItem(rand.nextInt(tw),rand.nextInt(th),rand.nextInt(8*3)+1)) break;
      }
    }
  }

  // all chests must be:
  // a) over a valid base tile
  // b) not on the edge of room boundaries
  // c) cannot be touching more than 1 fringe tile
  // d) not over or adjacent to another chest
  private final boolean newItem(int c, int r, int t) {
   int f = 0;
    if (
          (tlBase.getCell(c, r) <= 1) // not over a valid base tile
       || (c == 0) || (c == tw-1) // on the room's x boundary
       || (r == 0) || (r == th-1) // on the room's y boundary
    ) return false;
    // make sure the current tile doesn't already contain an item
    if (tlItems.getCell(c,r) != 0) return false;
    if (t/3 < 8) { // non-passive item tile, 7*3 chest tiles + 2 skeleton tiles
      // check all adjacent tiles (including the current tile) for a fringe tile or an existing chest
      for (int y=-1; y <= 1; y++) {
        for (int x=-1; x <= 1; x++) {
          if (tlFringe.getCell(c+x, r+y) == VOID_TILE) { // found a chest
            return false;
          } else if (tlFringe.getCell(c+x, r+y) != 0) { // found a fringe tile
            if (++f > 1) return false; // touching more than one
          }
        }
      }
      tlFringe.setCell(c, r, VOID_TILE);
      if (t <= 7*3) { // chest
        t = (((t-1)/3) * 3) + 1; // make sure it's a closed chest
        f = tlItems.createAnimatedTile(t);
        if ((maze.cellCurrent.items & (1 << -(f+1))) != 0) {
          tlItems.setAnimatedTile(f, t+2); // show opened chest
        }
      } else { // skeleton
        f = t;
      }
    } else { // adding passive (walkable) items (loot bag)
      if (tlFringe.getCell(c, r) != 0) return false;
      f = tlItems.createAnimatedTile(t);
      if ((maze.cellCurrent.items & (1 << -(f+1))) != 0) {
        tlItems.setAnimatedTile(f, 0); // item already taken
      }
    }
    tlItems.setCell(c, r, f);
    return true;
  }

  public final int getX() { return tlBase.getX(); }
  public final int getY() { return tlBase.getY(); }
  public final int getWidth() { return tlBase.getWidth(); }
  public final int getHeight() { return tlBase.getHeight(); }

  public final void setViewPort(int x, int y, int w, int h) {
    viewX = x-w/2;
    viewY = y-h/2;

    if (viewX < getX()) {
      viewX = getX();
    } else if (viewX > (getX()+getWidth()-w)) {
      viewX = getX()+getWidth()-w;
    }
    if (viewY < getY()) {
      viewY = getY();
    } else if (viewY > (getY()+getHeight()-h)) {
      viewY = getY()+getHeight()-h;
    }
    setViewWindow(viewX, viewY, w, h);
  }
  
  private static final void offsetAnimatedTile(TiledLayer layer, int tile, int offset) {
    layer.setAnimatedTile(tile, layer.getAnimatedTile(tile)+offset);
  }

  public final void doorOpen(int dir) {
   final int tile1, tile2;
    if (dir == MazeMap.LEFT) {
      tile1 = tlDoors.getCell(0,tlBase.getRows()/2-1);
      tile2 = tlDoors.getCell(0,tlBase.getRows()/2);
    } else if (dir == MazeMap.RIGHT) {
      tile1 = tlDoors.getCell(tlBase.getColumns()-1,tlBase.getRows()/2-1);
      tile2 = tlDoors.getCell(tlBase.getColumns()-1,tlBase.getRows()/2);
    } else if (dir == MazeMap.UP) {
      tile1 = tlDoors.getCell(tlBase.getColumns()/2-1,0);
      tile2 = tlDoors.getCell(tlBase.getColumns()/2,0);
    } else { // if (dir == MazeMap.DOWN) {
      tile1 = tlDoors.getCell(tlBase.getColumns()/2-1,tlBase.getRows()-1);
      tile2 = tlDoors.getCell(tlBase.getColumns()/2,tlBase.getRows()-1);
    }
    offsetAnimatedTile(tlDoors,tile1,+4);
    offsetAnimatedTile(tlDoors,tile2,+4);
  }
}