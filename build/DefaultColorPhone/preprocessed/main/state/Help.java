/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package main.state;

import javax.microedition.lcdui.Graphics;
import main.Engine;
import game.GameState;

/**
 *
 * @author Chris
 */
public final class Help extends GameState {
  public Help(Engine engine) { super(engine); }

  protected void stateAction() {
    if (Engine.keyUp(engine.aMENU)) { // aMENU released
      engine.SetState(engine.STATE_MENU);
    }
  }

  protected void stateUpdate() {
    engine.clearCanvas(0x000000);
    engine.font3.drawString(engine.gfx, "HELP", engine.getWidth()/2, 1, Graphics.HCENTER|Graphics.TOP);
    engine.drawSoftButtons(engine.font1,"BACK",null);
    engine.flushGraphics();
  }
}
