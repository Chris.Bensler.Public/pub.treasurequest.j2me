/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package main.state;

import java.io.IOException;
import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Image;
import javax.microedition.lcdui.game.Sprite;
import main.Engine;
import game.GameAction;
import game.GameSprite;
import game.GameState;

/**
 *
 * @author Chris
 */
public final class Play extends GameState {
 private final GameAction aMAP  = new GameAction(Engine.SOFT_RIGHT);
//#if DefaultConfiguration
//#  private final GameAction aDEBUG = new GameAction(Engine.KEY_POUND);
//#else
   private final GameAction aDEBUG = new GameAction(Engine.KEY_NUM9);
//#endif
 private final GameAction[] ACTION_PLAY = new GameAction[] {aMAP,aDEBUG};

 private Runtime rt = Runtime.getRuntime();
 private long tMem=0,fMem=0,tick=0;

 public boolean paused = false;
 
  public Play(Engine engine) { super(engine); }

  protected void start() {
    engine.SetAction(ACTION_PLAY);
    paused = false;
    if (engine.maze == null) {
      Runtime.getRuntime().gc();
      try {
        engine.tsMAP = Image.createImage("/img/tset_maze_map.png");
        engine.tsGround = Image.createImage("/img/ground.png");
        engine.tsFringe = Image.createImage("/img/fringe.png");
        engine.tsItems = Image.createImage("/img/items.png");
        engine.tsPC = Image.createImage("/img/char1.png");
        engine.pc = new GameSprite(engine, engine.tsPC,16,24);
        engine.pc.setRefPixelPosition(0, 16);
        engine.pc.setCollisionRect(2, 16, engine.pc.getWidth()-4, 8);
        // walk up,down,left,right
        engine.pc.setAnimateMove(new int[] {0, 1, 2, 1} ,new int[] {6, 7, 8, 7} ,new int[] {9, 10, 11, 10}, new int[] {3, 4, 5, 4});
        // stand up,down,left,right
        engine.pc.setAnimateIdle(new int[] {1}, new int[] {7}, new int[] {10}, new int[] {4});
        engine.pc.setIdle();
        engine.tsMOB_Skeleton = Image.createImage("/img/skeleton.png");
        engine.mobSkeleton = new GameSprite(engine, engine.tsMOB_Skeleton,20,24);
        engine.mobSkeleton.setRefPixelPosition(0, 16);
        engine.mobSkeleton.setCollisionRect(2, 16, engine.mobSkeleton.getWidth()-4, 8);
      } catch(Exception e) {
        e.printStackTrace();
        engine.exit();
      }
      System.gc();
      if (Engine.SEED_VALUE != 0) engine.rand.setSeed(Engine.SEED_VALUE);
      engine.pc.start();
      engine.newMaze();
      System.gc();
    }
  }

  protected void stop() {
    engine.RemoveAction(ACTION_PLAY);
    paused = true;
  }

  protected void stateAction() {
    if (Engine.keyUp(engine.aMENU)) { // aMENU released
      engine.STATE_MENU.selected = 0;
      engine.SetState(engine.STATE_MENU);
    } else if (Engine.keyUp(aMAP)) { // soft right
      engine.map_mode++;
      if (engine.map_mode == 1) {
        try {
          engine.tsHUD = Image.createImage("/img/tset_maze_hud12.png");
        } catch (IOException ex) {
          ex.printStackTrace();
        }
        engine.spriteHUD = new Sprite(engine.tsHUD,engine.tsHUD.getWidth()/15,engine.tsHUD.getWidth()/15);
      } else {
        engine.tsHUD = null;
        engine.spriteHUD = null;
        engine.map_mode = 0;
      }
    } else if (Engine.keyUp(aDEBUG)) {
      if (++engine.debug_flag > 4) engine.debug_flag = 0;
    } else {
      checkKeys();
    }
  }

  private void checkKeys() {
   double dx=0.0,dy=0.0;
    if (!paused && (engine.map_mode == 0)) {
      if (Engine.keyDown(engine.aUP)) {
        engine.pc.setMove(Engine.UP);
        dy = -GameSprite.SPEED*engine.fpsScale;
      } else if (Engine.keyDown(engine.aDOWN)) {
        engine.pc.setMove(Engine.DOWN);
        dy = GameSprite.SPEED*engine.fpsScale;
      } else if (Engine.keyDown(engine.aLEFT)) {
        engine.pc.setMove(Engine.LEFT);
        dx = -GameSprite.SPEED*engine.fpsScale;
      } else if (Engine.keyDown(engine.aRIGHT)) {
        engine.pc.setMove(Engine.RIGHT);
        dx = GameSprite.SPEED*engine.fpsScale;
      } else {
        engine.pc.setIdle();
        if (Engine.keyUp(engine.aFIRE)) {
         int w,h,c,r;
         int[] collide = engine.pc.getCollisionRect();
         int[] rect = new int[4];
          w = engine.room.tlBase.getCellWidth();
          h = engine.room.tlBase.getCellHeight();
          rect[0] = engine.pc.getX()-engine.room.getX() + collide[0] + (collide[2]/2)-1; // sprite axis
          rect[1] = engine.pc.getY()-engine.room.getY() + collide[1] + (collide[3]/2)-1;  // sprite axis
          rect[0] /= w;
          rect[1] /= h;
          for (r = rect[1]-1; r <= rect[1]+1; r++) {
            for (c = rect[0]-1; c <= rect[0]+1; c++) {
              if (
                    (
                        ((engine.pc.dir == Engine.LEFT)  && (c == rect[0]-1))
                     || ((engine.pc.dir == Engine.RIGHT) && (c == rect[0]+1))
                     || ((engine.pc.dir == Engine.UP)    && (r == rect[1]-1))
                     || ((engine.pc.dir == Engine.DOWN)  && (r == rect[1]+1))
                     || ((c == rect[0]) && (r == rect[1])) // tile the pc is standing on
                    )
                 && (engine.room.tlItems.getCell(c,r) < 0)
                 ) { // found something
                engine.action_item(c,r);
                return;
              }
            }
          }
        }
      }
      engine.movePC(dx,dy);
    }
  }

  protected void stateUpdate() {
    if (engine.first_pass) return;
    engine.clearCanvas(0x000000);
    if (engine.map_mode != 0) {
      engine.drawHUD();
    } else {
      engine.room.paint(engine.gfx,0,0);
    }
    engine.gfx.setColor(0xFFFFFF);
    engine.font1.drawString(engine.gfx,Long.toString((long) engine.fps),1,2,Graphics.LEFT|Graphics.TOP);
    int y = 10;
    for (int i=0; i < engine.txtGameInfo.size(); i++) {
      y += Engine.drawWrapped(engine.font3, engine.gfx, (String) engine.txtGameInfo.elementAt(i),10,y, engine.getWidth()-20 ,Graphics.LEFT|Graphics.TOP);
    }
    int i = 1;
    if (engine.debug_flag == 1) {
      engine.font1.drawString(engine.gfx,(tMem - fMem)+"/"+tMem+" ("+fMem+" free)",1,engine.getHeight()-10*i++,Graphics.LEFT|Graphics.BOTTOM);
      engine.font1.drawString(engine.gfx,"seed = "+engine.maze.SEED_VALUE, 1, engine.getHeight()-10*i++, Graphics.LEFT|Graphics.BOTTOM);
      engine.font1.drawString(engine.gfx,"Map["+engine.maze.cellCurrent.y+"]["+engine.maze.cellCurrent.x+"]",1,engine.getHeight()-10*i++,Graphics.LEFT|Graphics.BOTTOM);
    }
    engine.drawSoftButtons(engine.font1,"MENU","MAP");
    engine.flushGraphics();
    tick++;
    if (tick > 100) {
      tick = 0;
      rt.gc();
      tMem = rt.totalMemory();
      fMem = rt.freeMemory();
    }
  }
}
