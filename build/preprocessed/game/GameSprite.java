/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package game;

import java.util.Vector;
import javax.microedition.lcdui.Image;
import javax.microedition.lcdui.game.Sprite;
import main.Engine;

/**
 *
 * @author Chris
 */
public class GameSprite extends Sprite implements Runnable {
 private final int MAX_DIR = Math.max(Math.max(Math.max(Engine.UP,Engine.DOWN),Engine.LEFT),Engine.RIGHT)+1;
 public static final double SPEED = 4*1.5*(30.0/Engine.FPS); // guage at 30 fps
 private Image tset = null;
 private double x=0,y=0;
 public boolean idle = false; // walking or standing?
 public int dir = 0;
 private int delay = 0; // runtime variable, set from MoveDelay or IdleDelay
 private int[] collide = new int[4];
 public Object[] MoveAnimate = new Object[MAX_DIR];
 public Object[] IdleAnimate = new Object[MAX_DIR];
 private int[] MoveDelay = new int[MAX_DIR]; // framespeed in milliseconds
 private int[] IdleDelay = new int[MAX_DIR]; // framespeed in milliseconds
 public Vector inventory = new Vector();
 private Thread thread;
 private Engine engine;

  public GameSprite(Engine engine, Image tset) {
    this(engine, tset,tset.getWidth(),tset.getHeight());
  }

  public GameSprite(Engine engine, Image tset, int w, int h) {
    super(tset,w,h);
    this.engine = engine;
    MoveAnimate[Engine.UP] = new int[] {1};  MoveAnimate[Engine.DOWN] = new int[] {2};  MoveAnimate[Engine.LEFT] = new int[] {3};  MoveAnimate[Engine.RIGHT] = new int[] {4};
    IdleAnimate[Engine.UP] = new int[] {1};  IdleAnimate[Engine.DOWN] = new int[] {2};  IdleAnimate[Engine.LEFT] = new int[] {3};  IdleAnimate[Engine.RIGHT] = new int[] {4};
    MoveDelay[Engine.UP] = 200;       MoveDelay[Engine.DOWN] = 200;       MoveDelay[Engine.LEFT] = 200;       MoveDelay[Engine.RIGHT] = 200;
    IdleDelay[Engine.UP] = 200;       IdleDelay[Engine.DOWN] = 200;       IdleDelay[Engine.LEFT] = 200;       IdleDelay[Engine.RIGHT] = 200;
    collide = new int[] {0,0,w,h};
  }

  public GameSprite(Engine engine, GameSprite s) {
    super(s);
    this.engine = engine;
    this.tset = s.tset;
    this.x = s.x;
    this.y = s.y;
    this.idle = s.idle;
    this.dir = s.dir;
    this.delay = s.delay;
    this.collide = Engine.copyArray(s.collide); // int[]
    this.MoveAnimate = Engine.copyArray(s.MoveAnimate); // Object[(int[])]
    this.IdleAnimate = Engine.copyArray(s.IdleAnimate); // Object[(int[])]
    this.MoveDelay = Engine.copyArray(s.MoveDelay); // int[]
    this.IdleDelay = Engine.copyArray(s.IdleDelay); // int[]
//        this.inventory = s.inventory; // copy?
//      this.thread = s.thread; // do not copy!
  }

  public void start() {
    thread = new Thread(this);
    thread.start();
  }

  public void setMove(int d) {
    if (idle || (d != dir)) {
      dir = d;
      setFrameSequence((int []) MoveAnimate[dir]);
      delay = MoveDelay[dir];
      idle = false;
    }
  }

  public void setIdle() {
    if (!idle) {
      if (dir == 0) dir = Engine.DOWN;
      setFrameSequence((int[]) IdleAnimate[dir]);
      delay = IdleDelay[dir];
      idle = true;
    }
  }

  public void setPosition(double x, double y) {
    this.x = x;
    this.y = y;
    super.setPosition((int) this.x, (int) this.y);
  }

  public void setPosition(int x, int y) {
    setPosition((double) x, (double) y);
  }

  public void setCollisionRect(int x, int y, int w, int h) {
    collide = new int[] {x,y,w,h};
    defineCollisionRectangle(x,y,w,h);
  }

  public int[] getCollisionRect() {
    return collide;
  }

  public void move(double dx, double dy) {
    setPosition(this.x+dx,this.y+dy);
  }

  public void move(int x, int y) {
    move((double) x, (double) y);
  }

  public void run() {
    while (engine.running) {
      if (!engine.STATE_PLAY.paused) {
        nextFrame();
      }
      try {
        Thread.sleep(delay);
      } catch(Exception e) {
        e.printStackTrace();
      }
    }
  }
}
