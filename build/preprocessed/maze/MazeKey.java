/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package maze;

import java.util.Random;

/**
 *
 * @author Chris
 */
public class MazeKey {
  public String description = "";
  public String name = "";
  public MazeCell drop = null;   // item (key) drop cell ref
  public MazeCell cell1 = null; // target door cell1 ref (front side of door)
  public MazeCell cell2 = null; // target door cell2 ref (back side of door) needed to handle mazes with intersecting paths
  public int lock1 = 0; // which door in the target room does this key unlock (LEFT,RIGHT,TOP,BOTTOM)
  public int lock2 = 0; // which door in the target room does this key unlock (LEFT,RIGHT,TOP,BOTTOM)
  protected int id = 0;

  protected MazeKey(int id, MazeCell cell1, int lock1, MazeCell cell2, int lock2) {
    this.lock1 = lock1;
    this.lock2 = lock2;
    this.cell1 = cell1;
    this.cell2 = cell2;
    this.id = id;
  }

  // big or small
  // plain looking? or intricately carved?
  // new/shiny or old/tarnished/rusty/rusted
  // Copper/Iron/Bronze/Steel/Silver/Gold/Platinum
  private static final String[] KEY_NAME1 = {
   "tiny"
  ,"small"
  ,"large"
  };

  private static final String[] KEY_NAME2 = {
   "old"
  ,"dirty"
  ,"dirty old"
  ,"finely decorated"
  ,"intricately carved"
  ,"intricately carved and decorated"
  };

  private static final String[] KEY_TYPE = {
   "copper"
  ,"bronze"
  ,"iron"
  ,"steel"
  ,"silver"
  ,"gold"
  ,"ivory"
  ,"runestone"
  };
 
  private final String setDescription(Random rand) {
   int n1 = rand.nextInt(KEY_NAME1.length*2);
   int n2 = rand.nextInt(KEY_NAME2.length*2);
   String kname = "";
    if (n1 < KEY_NAME1.length) kname = KEY_NAME1[n1];
    if (n2 < KEY_NAME2.length) {
      if (kname.length() == 0) {
        kname = KEY_NAME2[n2];
      } else {
        kname = kname+", "+KEY_NAME2[n2];
      }
    }
    if (kname.length() != 0) kname = kname+" ";
    return kname;
  }
  
  protected void setKey(Random rand, int i) {
    cell1.lock = cell1.lock | lock1 | (i << (8+4*(lock1 >> 1)));
    cell2.lock = cell2.lock | lock2 | (i << (8+4*(lock2 >> 1)));
    name = KEY_TYPE[i];
    setDescription(rand);
  }
}

