/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package maze;

import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Vector;
import javax.microedition.lcdui.Display;
import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Image;
import javax.microedition.lcdui.game.GameCanvas;
import javax.microedition.lcdui.game.Sprite;
import javax.microedition.midlet.*;
import maze.DungeonMaze.MazeKey;
import maze.DungeonMaze.MazeMap;
import maze.DungeonMaze.MazeRoom;

/**
 * @author Chris
 */
public class MazeTest extends MIDlet {
 Display display;
 MazeCanvas canvas;

  public void startApp() {
    display = Display.getDisplay(this);
    canvas = new MazeCanvas();
    canvas.setFullScreenMode(true);
    display.setCurrent(canvas);
    canvas.start();
  }

  public void pauseApp() {
  }

  public void destroyApp(boolean unconditional) {
    notifyDestroyed();
  }
  
  public class MazeCanvas extends GameCanvas implements Runnable {
   final int MAX_MAZEX = 14;
   final int MAX_MAZEY = 14;
   // RATIOS are int values in the range of 0 to 100 representing a percentage chance of that behaviour
   final int DIR_RATIO = 60; // controls the tendency to continue going the same direction
   final int VISIT_RATIO = 40; // controls the tendency to visit neighbouring cells, setting this too low will produce small and unusable mazes
   final int FILL_RATIO = 60; // controls how much of the specified region it will try to fill in
   final int GAP_RATIO = 30; // controls the tendency to create extra gaps (doorways)
   final int LOCK_RATIO = 100; // controls the tendency to lock a door (max of 8 locked doors per maze)
   final int SEED_VALUE = 2051716863; // any value other than 0 will preset the RNG for the maze
   final int MAX_REDO = 10; // number of tries to maximize the fill ratio
   final int MIN_AREA = Math.max(MAX_MAZEX, MAX_MAZEY)+1; // minimum number of valid cells

   Graphics gfx;
   int width,height;
   int viewX,viewY,viewW,viewH;
   public MazeMap maze = null;
   public MazeRoom room = null;
   public Image tsGround,tsFringe,tsItems,tsMAP,tsHUD,tsCharPC;
   public Sprite spriteMAP,spriteHUD,spritePC;
   public SpriteManager pc;
   Random rand = new Random();
   Thread thread;
   Timer timer;

   Vector txtGameInfo = new Vector();
   boolean stopped = false;
   boolean paused = false;
   int debug_flag = 0;
   int map_mode = 0;
   
   boolean first_pass = true;

    public MazeCanvas() {
      super(false);
      width = getWidth();
      height = getHeight();
      viewX = 0;
      viewY = 0;
      viewW = width;
      viewH = height;
    }
    
    public void sizeChanged(int w, int h) {
      width = w;
      height = h;
      viewW = w;
      viewH = h;
    }
    
    public void start() {
      try {
        tsMAP = Image.createImage("/maze/tset_maze_map.png");
        spriteMAP = new Sprite(tsMAP,tsMAP.getWidth()/15,tsMAP.getWidth()/15);
        tsHUD  = Image.createImage("/maze/tset_maze_hud12.png");
        spriteHUD = new Sprite(tsHUD,tsHUD.getWidth()/15,tsHUD.getWidth()/15);
        tsGround = Image.createImage("/maze/ground.png");
        tsFringe = Image.createImage("/maze/fringe.png");
        tsItems = Image.createImage("/maze/items.png");
        tsCharPC = Image.createImage("/maze/char1.png");
        spritePC = new Sprite(tsCharPC,16,24);
        pc = new SpriteManager(spritePC);
        pc.sprite.setRefPixelPosition(0, 16);
        pc.setCollisionRect(2, 16, pc.getWidth()-4, 8);
        // walk up,down,left,right
        pc.setAnimateMove(new int[] {0, 1, 2, 1} ,new int[] {6, 7, 8, 7} ,new int[] {9, 10, 11, 10}, new int[] {3, 4, 5, 4});
        pc.setAnimateMoveDelay(200,200,200,200);
        // stand up,down,left,right
        pc.setAnimateIdle(new int[] {1}, new int[] {7}, new int[] {10}, new int[] {4});
        pc.setAnimateIdleDelay(200,200,200,200);
        pc.setIdle();
      } catch(Exception e) {
        e.printStackTrace();
        destroyApp(true);
      }
      if (SEED_VALUE != 0) rand.setSeed(SEED_VALUE);
      timer = new Timer();
      thread = new Thread(this);
      thread.start();
      pc.start();
    }
    
    public void stop() {
      stopped = true;
    }
    
    public void exit() {
      tsMAP = null;
      tsHUD = null;
      spriteMAP = null;
      spriteHUD = null;
      thread = null;
      destroyApp(false);
    }
    
    public void newMaze() {
     MazeMap mazeRedo;
     int redo = MAX_REDO;
     int x,y;
      maze = new MazeMap(spriteMAP,MAX_MAZEX,MAX_MAZEY,DIR_RATIO,GAP_RATIO,VISIT_RATIO,FILL_RATIO,LOCK_RATIO,(first_pass && (SEED_VALUE != 0)) ? SEED_VALUE : rand.nextInt());
      while (((redo > 0) || (maze.mazeArea == 1)) && ((MIN_AREA > maze.mazeArea) || (maze.mazeArea < (FILL_RATIO/100 * maze.XMAX * maze.YMAX)))) {
        redo--;
        mazeRedo = new MazeMap(spriteMAP,MAX_MAZEX,MAX_MAZEY,DIR_RATIO,GAP_RATIO,VISIT_RATIO,FILL_RATIO,LOCK_RATIO,rand.nextInt());
        if (maze.mazeArea < mazeRedo.mazeArea) {
          maze = null;
          System.gc();
          maze = mazeRedo;
        }
        mazeRedo = null;
        System.gc();
      }
      room = new MazeRoom(maze,tsMAP,tsGround,tsFringe,tsItems);
      room.insert(spritePC,0);
      // place the pc
      while (true) {
        x = rand.nextInt(room.tlBase.getColumns());
        y = rand.nextInt(room.tlBase.getRows());
        if ((room.tlBase.getCell(x, y) > 1) && (room.tlFringe.getCell(x,y) == 0) && (room.tlItems.getCell(x, y) == 0)) {
          x *= room.tlBase.getCellWidth();
          y *= room.tlBase.getCellHeight();
          pc.sprite.setPosition(x,y+room.tlBase.getCellHeight()-pc.getHeight());
          break;
        }
      }
      room.setViewPort(pc.getX()+pc.getWidth()/2, pc.getY()+pc.getHeight()/2,viewW,viewH);
      first_pass = false;
    }

    boolean door_locked_wait = false;
    public boolean needKey(int dir) {
     MazeKey key = null;
      if (maze.getLockDir(dir) == 0) return false; // not locked
      key = (MazeKey) maze.keys.elementAt(maze.getLockKey(dir));
      // need key
      if (!door_locked_wait) {
        txtGameInfo.addElement("A door blocks your path.\n"
                +"There is a "+key.description+key.name
                +" lock with the number "+(maze.getLockKey(dir)+1)+" etched into it.");
        door_locked_wait = true;
        timer.schedule(new DoorLockedTimeout(), 3000);
      }
      if (pc.inventory.indexOf(key) != -1) { // has the key
        pc.inventory.removeElement(key);
        room.doorOpen(dir);
        txtGameInfo.addElement("You use the "+key.name+" key to unlock the door.");
        timer.schedule(new DoorLockedTimeout(), 3000);
        key.cell1.lock -= key.lock1;
        key.cell2.lock -= key.lock2;
        return false;
      }
      return true;
    }

    class DoorLockedTimeout extends GameInfoTimeout {
      public void run() {
        super.run();
        door_locked_wait = false;
      }
    }

    public boolean nextRoom() {
     int c = maze.cellCurrent.x;
     int r = maze.cellCurrent.y;
      // check if we need to move to next room
      if (!pc.sprite.collidesWith(room.tlDoors, false)) return false;
      if (pc.dir == LEFT) {
        if (needKey(maze.LEFT)) return false;
        c--;
        pc.sprite.move(+room.getWidth()-2*room.tlBase.getCellWidth()-pc.getWidth()+4,0);
      } else if (pc.dir == RIGHT) {
        if (needKey(maze.RIGHT)) return false;
        c++;
        pc.sprite.move(-room.getWidth()+2*room.tlBase.getCellWidth()+pc.getWidth()-4,0);
      } else if (pc.dir == UP) {
        if (needKey(maze.UP)) return false;
        r--;
        pc.sprite.move(0,+room.getHeight()-2*room.tlBase.getCellHeight()-pc.getHeight()+16);
      } else if (pc.dir == DOWN) {
        if (needKey(maze.DOWN)) return false;
        r++;
        pc.sprite.move(0,-room.getHeight()+2*room.tlBase.getCellHeight()+pc.getHeight()-16);
      }
      maze.cellCurrent = maze.Map[r][c];
      room = null;
      room = new MazeRoom(maze,tsMAP,tsGround,tsFringe,tsItems);
      room.insert(spritePC,0);
      return true;
    }

    public void movePC(int dx, int dy) {
      if (!pc.idle) {
        pc.sprite.move(dx,dy);
        if (!nextRoom() && pc.sprite.collidesWith(room.tlFringe, false)) {
          pc.sprite.move(-dx,-dy);
        }
        room.setViewPort(pc.getX()+pc.getWidth()/2,pc.getY()+pc.getHeight()/2,viewW,viewH);
      }
    }
  
    public void keyPressed(int key) { pressed(key); }
    public void keyRepeated(int key) { pressed(key); }
    public void pressed(int key) {}
    
    public void keyReleased(int key) {
     int action = getGameAction(key);
      if (key == -21) { // soft left
        stop();
      } else if (key == -22) { // soft right
        map_mode++;
        if (map_mode != 1) map_mode = 0;
      } else if (key == KEY_STAR) {
        newMaze();
      } else if (key == KEY_POUND) {
        if (++debug_flag > 4) debug_flag = 0;
      } else if (map_mode == 0) {
        if (action == FIRE) {
         int w,h,c,r;
         int[] collide = pc.getCollisionRect();
         int[] rect = new int[4];
          w = room.tlBase.getCellWidth();
          h = room.tlBase.getCellHeight();
          rect[0] = pc.getX()-room.getX() + collide[0] + (collide[2]/2)-1; // sprite axis
          rect[1] = pc.getY()-room.getY() + collide[1] + (collide[3]/2)-1;  // sprite axis
          rect[0] /= w;
          rect[1] /= h;
          for (r = rect[1]-1; r <= rect[1]+1; r++) {
            for (c = rect[0]-1; c <= rect[0]+1; c++) {
              if (
                    (
                        ((pc.dir == LEFT)  && (c == rect[0]-1))
                     || ((pc.dir == RIGHT) && (c == rect[0]+1))
                     || ((pc.dir == UP)    && (r == rect[1]-1))
                     || ((pc.dir == DOWN)  && (r == rect[1]+1))
                     || ((c == rect[0]) && (r == rect[1])) // tile the pc is standing on
                    )
                 && (room.tlItems.getCell(c,r) < 0)
                 ) { // found something
                action_item(c,r);
                return;
              }
            }
          }
        }
      }
    }
    
    class GameInfoTimeout extends TimerTask {
      public void run() {
        txtGameInfo.removeAllElements();
      }
    }

    void action_item(int c, int r) {
     // open
     int item = -(room.tlItems.getCell(c, r)+1);
      if (( maze.cellCurrent.items & (1 << item)) == 0) {
        maze.cellCurrent.items |= (1 << item);
        new AnimateItem(item);
      }
    }

    class AnimateItem implements Runnable {
     int item,tile,last;
     MazeKey key;
      public AnimateItem(int item) {
        this.item = -(item+1);
        this.tile = room.tlItems.getAnimatedTile(this.item);
        if (this.tile <= 7*3) { // chest
          this.last = this.tile+2;
          run();
        } else {
          if (this.tile >= 25) { // a key
            // add to player inventory
            key = (MazeKey) maze.keys.elementAt(tile-25);
            pc.inventory.addElement(key);
            txtGameInfo.addElement("You found a "+key.description+key.name+" key.\n"
                    + "There is a number "+(tile-25+1)+" etched into it.");
            timer.schedule(new GameInfoTimeout(), 3000);
          }
          room.tlItems.setAnimatedTile(this.item, 0);
        }
      }

      public void run() {
        while (this.tile++ != this.last) {
          room.tlItems.setAnimatedTile(this.item, this.tile);
          try {
            Thread.sleep(100);
          } catch (InterruptedException ex) {
            ex.printStackTrace();
          }
        }
      }
    }
    
    public void checkKeyState() {
     int dx=0,dy=0;
     int key;
      key = getKeyStates();
      if (map_mode == 0) {
        if ((key & UP_PRESSED) != 0) {
          pc.setMove(UP);
          dy = -pc.SPEED;
        } else if ((key & DOWN_PRESSED) != 0) {
          pc.setMove(DOWN);
          dy = pc.SPEED;
        } else if ((key & LEFT_PRESSED) != 0) {
          pc.setMove(LEFT);
          dx = -pc.SPEED;
        } else if ((key & RIGHT_PRESSED) != 0) {
          pc.setMove(RIGHT);
          dx = pc.SPEED;
        } else {
          pc.setIdle();
        }
        movePC(dx,dy);
      } else {
      }
    }
    
    public void drawHUD() {
     int ox,oy,x,y,w,h;
     MazeKey key;
      w = spriteHUD.getWidth();
      h = spriteHUD.getHeight();
      ox = (width-maze.XMAX*w)/2;
      oy = (height-maze.YMAX*h)/2;
      maze.draw(gfx,spriteHUD,ox,oy, debug_flag);

      // mark start room
      gfx.setColor(0xFFFF00);
      gfx.drawRect(ox+maze.cellStart.x*w-1, oy+maze.cellStart.y*h-1, w+1, h+1);

      // mark end room
      gfx.setColor(0x0000FF);
      gfx.drawRect(ox+maze.cellFinish.x*w-1, oy+maze.cellFinish.y*h-1, w+1, h+1);

      // mark current room
      gfx.setColor(0xFFFFFF);
      gfx.drawRect(ox+maze.cellCurrent.x*w-1, oy+maze.cellCurrent.y*h-1, w+1, h+1);

      for (y=0; y < maze.YMAX; y++) {
        for (x=0; x < maze.XMAX; x++) {
          if (maze.Map[y][x].visited) {
            if (debug_flag == 2) {
              gfx.drawString(""+maze.Map[y][x].count, ox+x*w-1, oy+y*h-1, 0);
            } else if (debug_flag == 3) {
              gfx.drawString(""+maze.Map[y][x].score, ox+x*w-1, oy+y*h-1, 0);
            } else if (debug_flag == 4) {
              gfx.drawString(""+(maze.Map[y][x].count*maze.Map[y][x].score), ox+x*w-1, oy+y*h-1, 0);
            }
          }
        }
      }

      // mark each door and each key
      for (int i=0; i < maze.keys.size(); i++) {
        gfx.setColor(0xFFFF00);
        key = (MazeKey) maze.keys.elementAt(i);
        if ((debug_flag != 0) || key.drop.visible) {
          x = ox + key.drop.x*w - 1;
          y = oy + key.drop.y*h - 1;
          gfx.drawRect(x+w/2, y+h/2, 1, 1);
        }
        if ((debug_flag != 0) || key.cell1.visible) {
          gfx.setColor(0x00FF00);
          x = ox + key.cell1.x*w - 1;
          y = oy + key.cell1.y*h - 1;
          if ((key.lock1 & maze.LEFT)  == maze.LEFT)  gfx.drawRect(x,       y+h/2, 1, 1);
          if ((key.lock1 & maze.RIGHT) == maze.RIGHT) gfx.drawRect(x+w-1,   y+h/2, 1, 1);
          if ((key.lock1 & maze.UP)    == maze.UP)    gfx.drawRect(x+w/2,   y, 1, 1);
          if ((key.lock1 & maze.DOWN)  == maze.DOWN)  gfx.drawRect(x+w/2,   y+h-1, 1, 1);
        }
        if ((debug_flag >= 2) || key.cell2.visible) {
          gfx.setColor(0x00FF00);
          x = ox + key.cell2.x*w - 1;
          y = oy + key.cell2.y*h - 1;
          if ((key.lock2 & maze.LEFT)  == maze.LEFT)  gfx.drawRect(x,       y+h/2, 1, 1);
          if ((key.lock2 & maze.RIGHT) == maze.RIGHT) gfx.drawRect(x+w-1,   y+h/2, 1, 1);
          if ((key.lock2 & maze.UP)    == maze.UP)    gfx.drawRect(x+w/2,   y, 1, 1);
          if ((key.lock2 & maze.DOWN)  == maze.DOWN)  gfx.drawRect(x+w/2,   y+h-1, 1, 1);
        }
      }
    }
    
    public int drawWrapped(String txt, int x, int y, int len, int align) {
     int start = 0, stop = 0, iy = 0;
      align = (align & (Graphics.LEFT|Graphics.RIGHT)) | Graphics.TOP; // force top alignment, retain specified horizontal alignment
      for (int i=0; i < txt.length(); i++) {
        if (txt.charAt(i) <= ' ') stop = i;
        if (((i-start) >= len) || (txt.charAt(i) == '\n')) {
          if (stop == start) stop = i;
          gfx.drawString(txt.substring(start, stop), x, y+10*iy++, align);
          if ((txt.charAt(i) == '\n') || (stop != i)) stop++;
          start = stop;
        }
      }
      if (start != txt.length()) gfx.drawString(txt.substring(start, txt.length()), x, y+10*iy++, align);
      return iy;
    }

    public void run() {
     Runtime rt = Runtime.getRuntime();
     double fps = 0;
     long startTime,endTime;
     long tMem=0,fMem=0,tick=0;
      gfx = getGraphics();
      newMaze();
      while (!stopped) {
        startTime = System.currentTimeMillis();
        checkKeyState();
        gfx.setColor(0x000000);
        gfx.fillRect(0, 0, width, height);
        if (map_mode != 0) {
          drawHUD();
        } else {
          room.paint(gfx,0,0);
        }
        gfx.setColor(0xFFFFFF);
        int y = 0;
        for (int i=0; i < txtGameInfo.size(); i++) {
          y += drawWrapped((String) txtGameInfo.elementAt(i),1,1+(1+10*(y)), 30 ,Graphics.LEFT|Graphics.TOP);
        }
        int i = 0;
        if (debug_flag == 1) {
          gfx.drawString((tMem - fMem)+"/"+tMem+" ("+fMem+" free)",1,height-10*i++,Graphics.LEFT|Graphics.BOTTOM);
          gfx.drawString("seed = "+maze.SEED_VALUE, 1, height-10*i++, Graphics.LEFT|Graphics.BOTTOM);
          gfx.drawString("Map["+maze.cellCurrent.y+"]["+maze.cellCurrent.x+"]",1,height-10*i++,Graphics.LEFT|Graphics.BOTTOM);
          gfx.drawString(""+fps,1,height-10*i++,Graphics.LEFT|Graphics.BOTTOM);
        }
        flushGraphics();
        try {
          Thread.sleep(20);
        } catch(Exception e) {
          e.printStackTrace();
          destroyApp(true);
        }
        endTime = System.currentTimeMillis();
        fps = 1000/(endTime-startTime);
        tick++;
        if (tick > 100) {
          tick = 0;
          rt.gc();
          tMem = rt.totalMemory();
          fMem = rt.freeMemory();
        }
      }
      exit();
    }
  
    public class SpriteManager extends Thread {
     public Sprite sprite;
     public int dir = 0;
     public boolean idle = false;
     private int delay = 0;
     private final int MAX_DIR = Math.max(Math.max(Math.max(UP,DOWN),LEFT),RIGHT)+1;
     private Object[] aniMove = new Object[MAX_DIR];
     private int[] aniMoveDelay = new int[MAX_DIR];
     private Object[] aniIdle = new Object[MAX_DIR];
     private int[] aniIdleDelay = new int[MAX_DIR];
     private int[] collide = new int[4];
     private int SPEED = 2;
     public Vector inventory = new Vector();

      public SpriteManager(Sprite s) {
        sprite = s;
      }

      public void setAnimateMove(int[] up, int[] down, int[] left, int[] right) {
        aniMove[UP] = up;
        aniMove[DOWN] = down;
        aniMove[LEFT] = left;
        aniMove[RIGHT] = right;
      }

      public void setAnimateMoveDelay(int up, int down, int left, int right) {
        aniMoveDelay[UP] = up;
        aniMoveDelay[DOWN] = down;
        aniMoveDelay[LEFT] = left;
        aniMoveDelay[RIGHT] = right;
      }

      public void setAnimateIdle(int[] up, int[] down, int[] left, int[] right) {
        aniIdle[UP] = up;
        aniIdle[DOWN] = down;
        aniIdle[LEFT] = left;
        aniIdle[RIGHT] = right;
      }

      public void setAnimateIdleDelay(int up, int down, int left, int right) {
        aniIdleDelay[UP] = up;
        aniIdleDelay[DOWN] = down;
        aniIdleDelay[LEFT] = left;
        aniIdleDelay[RIGHT] = right;
      }

      public void setMove(int d) {
        if (idle || (d != dir)) {
          dir = d;
          sprite.setFrameSequence((int []) aniMove[dir]);
          delay = aniMoveDelay[dir];
          idle = false;
        }
      }

      public void setIdle() {
        if (!idle) {
          if (dir == 0) dir = DOWN;
          sprite.setFrameSequence((int[]) aniIdle[dir]);
          delay = aniIdleDelay[dir];
          idle = true;
        }
      }

      public int getX() { return sprite.getX(); }
      public int getY() { return sprite.getY(); }
      public int getWidth() { return sprite.getWidth(); }
      public int getHeight() { return sprite.getHeight(); }

      public void setCollisionRect(int x, int y, int w, int h) {
        collide[0] = x;
        collide[1] = y;
        collide[2] = w;
        collide[3] = h;
        sprite.defineCollisionRectangle(x,y,w,h);
      }

      public int[] getCollisionRect() {
        return collide;
      }

      public void run() {
        while (!stopped) {
          if (!paused) {
            sprite.nextFrame();
          }
          try {
            sleep(delay);
          } catch(Exception e) {
            e.printStackTrace();
          }
        }
      }
    }
  }
}
