/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package maze;

import java.util.Random;
import java.util.Vector;
import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.game.Sprite;

/**
 *
 * @author Chris
 */
public class MazeMap {
 public static final int LEFT    = 1; // 1 << 0 = 1
 public static final int UP      = 2; // 1 << 1 = 2
 public static final int RIGHT   = 4; // 1 << 2 = 4
 public static final int DOWN    = 8; // 1 << 3 = 8
 
 private final int MAX_KEYS = 8; // key index must fit in 1 byte. upper limit is 16 keys (0 to 15)
 private final int MAX_KEYQUEUE = 8; // max consecutive number of doors that will be locked before dropping a key

 public static final int tw = 15; // number of tiles per row

 public MazeCell[][] Map;
 public MazeCell cellStart;
 public MazeCell cellFinish;
 public MazeCell cellCurrent;
 private int mazeCount = 0;
 public int mazeArea = 0;
 private int mazeDir = 0;
 public Vector keys = new Vector();
 private Vector keyQueue = new Vector();

 public int XMAX;
 public int YMAX;
 private int UNIT_SIZE;
 
 private float DIR_RATIO;
 private float GAP_RATIO;
 private float VISIT_RATIO;
 private float FILL_RATIO;
 private float LOCK_RATIO;
 
 public int SEED_VALUE;
 private Random rand = new Random();

 private int next_key_id = 0;

  public MazeMap(Sprite tileset, int xmax, int ymax, int dir_ratio, int gap_ratio, int visit_ratio, int fill_ratio, int lock_ratio, int seed) {
   int x,y, th;
    th = (int) tileset.getRawFrameCount()/tw;
    XMAX = xmax;
    YMAX = ymax;
    UNIT_SIZE = tileset.getWidth();
    DIR_RATIO = dir_ratio;
    GAP_RATIO = gap_ratio;
    VISIT_RATIO = visit_ratio;
    FILL_RATIO = fill_ratio;
    LOCK_RATIO = lock_ratio;
    SEED_VALUE = seed;
    if (seed != 0) rand.setSeed(seed);
    mazeDir = (1 << rand.nextInt(4));

    Map = new MazeCell[YMAX][XMAX];
    for (y=0; y < YMAX; y++) for (x=0; x < XMAX; x++) {
      Map[y][x] = new MazeCell();
      Map[y][x].x = x;
      Map[y][x].y = y;
    }
    
    x = rand.nextInt(XMAX);
    y = rand.nextInt(YMAX);
    cellStart = Map[y][x];
    cellCurrent = cellStart;
    cellFinish = cellStart;
    visit_cell(cellCurrent,cellCurrent);
    
    // correct the cell counts
    int low = 0;
    boolean loop = true;
    while (loop) {
      loop = false;
      for (y=0; y < YMAX; y++) {
        for (x=0; x < XMAX; x++) {
          low = Map[y][x].count;
          if (((Map[y][x].gap & LEFT)   != 0) && (x > 0)      && Map[y][x-1].visited && (Map[y][x-1].count < low)) low = Map[y][x-1].count+1;
          if (((Map[y][x].gap & UP)     != 0) && (y > 0)      && Map[y-1][x].visited && (Map[y-1][x].count < low)) low = Map[y-1][x].count+1;
          if (((Map[y][x].gap & RIGHT)  != 0) && (x < XMAX-1) && Map[y][x+1].visited && (Map[y][x+1].count < low)) low = Map[y][x+1].count+1;
          if (((Map[y][x].gap & DOWN)   != 0) && (y < YMAX-1) && Map[y+1][x].visited && (Map[y+1][x].count < low)) low = Map[y+1][x].count+1;
          if (Map[y][x].count != low) {
            Map[y][x].count = low;
            loop = true;
          }
        }
      }
    }

    MazeKey key;
    int valid = 0;
    while (!keyQueue.isEmpty()) {
      x = rand.nextInt(XMAX);
      y = rand.nextInt(YMAX);
      if ((Map[y][x].visited) && ((Map[y][x].keys - (Map[y][x].keys & valid)) == 0)) { // this cell is valid
        key = (MazeKey) keyQueue.elementAt(rand.nextInt(keyQueue.size()));
        valid |= (1 << key.id);
        key.drop = Map[y][x];
        keys.addElement(key);
        keyQueue.removeElement(key);
      }
    }

    // final pass, select tiles, score cells, choose cellFinish
    rand.setSeed(SEED_VALUE+th);
    for (y=0; y < YMAX; y++) {
      for (x=0; x < XMAX; x++) {
        if (Map[y][x].visited) {

          // select the cell tile
          Map[y][x].framex = 0;
          if ((Map[y][x].gap & LEFT) != 0)   Map[y][x].framex |= LEFT;
          if ((Map[y][x].gap & UP) != 0)     Map[y][x].framex |= UP;
          if ((Map[y][x].gap & RIGHT) != 0)  Map[y][x].framex |= RIGHT;
          if ((Map[y][x].gap & DOWN) != 0)   Map[y][x].framex |= DOWN;
          Map[y][x].framex--;
          Map[y][x].framey = rand.nextInt(th);
          if (Map[y][x].framex < 0) {
            Map[y][x].visited = false;
            Map[y][x].framex = (LEFT|RIGHT|UP|DOWN)-1;
          } else {
            Map[y][x].frame = tw*Map[y][x].framey+Map[y][x].framex;
          }

          // calculate cell score
          setCellScore(Map[y][x],0);
          
          // determine cellFinish
          if (Map[y][x].count*Map[y][x].score > cellFinish.count*cellFinish.score) {
            cellFinish = Map[y][x];
          }

        }
      }
    }
    cellFinish.visited = true;

    keyQueue.removeAllElements();
    for (int i=keys.size()-1; i >= 0; i--) {
      key = (MazeKey) keys.elementAt(i);
      key.setKey(rand, keyQueue.size());
      keyQueue.addElement(key);
    }
    keys = keyQueue;
    keyQueue = null;
  }

  private MazeKey getKeyFromID(int id) {
   MazeKey key;
    for (int i=0; i < keys.size(); i++) {
      key = (MazeKey) keys.elementAt(i);
      if (key.id == id) return key;
    }
    for (int i=0; i < keyQueue.size(); i++) {
      key = (MazeKey) keyQueue.elementAt(i);
      if (key.id == id) return key;
    }
    return null;
  }
  
  private int setCellScore(MazeCell cell, int ignore) {
   int k = cell.keys;
   int id;
    k -= (k & ignore);
    cell.score = 1;
    for (int i=0; i < 8; i++) {
      id = (k & (1 << i)) >> i;
      if (id != 0) {
        cell.score += setCellScore(getKeyFromID(id).drop, cell.keys | ignore);
      }
    }
    return cell.score;
  }
 
  private int[] scramble(int[] list) {
   Vector tmp = new Vector();
   int[] ret = new int[list.length];
   int r;
    for (int i=0; i < list.length; i++) {
      r = (tmp.size() == 0) ? 0 : rand.nextInt(tmp.size()+1);
      if (r == tmp.size()) {
        tmp.addElement(new Integer(list[i]));
      } else {
        tmp.insertElementAt(new Integer(list[i]), r);
      }
    }
    for (int i=0; i < tmp.size(); i++) {
      ret[i] = ((Integer) tmp.elementAt(i)).intValue();
    }
    tmp = null;
    return ret;
  }

  private boolean visited_cell(MazeCell cell) {
    if (cell.visited) return true;
    if (mazeArea >= (FILL_RATIO/100 * XMAX * YMAX)) return true;
    return false;
  }

  private void visit_cell(MazeCell last_cell, MazeCell cell) {
   int[] id;
    if (visited_cell(cell)) return;
  
    mazeArea++;
    cell.visited = true;
    cell.count = mazeCount++;

    if (DIR_RATIO > rand.nextInt(100)) { // chance to continue in same direction, making longer corridors
      xvisit_cell(last_cell,mazeDir,cell,true);
    }
    
    id = scramble(new int[] {LEFT,UP,RIGHT,DOWN});
    for (int i=0; i < 4; i++) {
      if (VISIT_RATIO > rand.nextInt(100)) {
        xvisit_cell(last_cell,id[i],cell,true);
      }
    }
    for (int i=0; i < 4; i++) {
      if (GAP_RATIO > rand.nextInt(100)) {
        xvisit_cell(last_cell,id[i],cell,false);
      }
    }
    mazeCount--;
  }
  
  private void lock_door(int lock1, int lock2, MazeCell cell1, MazeCell cell2) {
   MazeKey key;
   int chance = 0;
    cell2.score = cell1.score;
    cell2.keys = cell1.keys;
    if (!visited_cell(cell2) && (keys.size()+keyQueue.size() < MAX_KEYS) && (keyQueue.size() < MAX_KEYQUEUE)) {
      chance = (int) ((XMAX*YMAX*(FILL_RATIO/100.0)));
      if ((LOCK_RATIO/100.0) * mazeArea/8.0 > rand.nextInt(chance)) {
        key = new MazeKey(++next_key_id, cell1, lock1, cell2, lock2);
        keyQueue.addElement(key);
        cell2.score++;
        cell2.keys |= (1 << key.id);
      }
    }
  }

  private void xvisit_cell(MazeCell last_cell, int dir, MazeCell cell, boolean visit) {
    if (dir == LEFT) {
      lvisit_cell(last_cell,cell,visit);
    } else if (dir == UP) {
      tvisit_cell(last_cell,cell,visit);
    } else if (dir == RIGHT) {
      rvisit_cell(last_cell,cell,visit);
    } else if (dir == DOWN) {
      bvisit_cell(last_cell,cell,visit);
    }
  }
  
  private boolean valid_cell(MazeCell last_cell, MazeCell cell1, MazeCell cell2, boolean visit) {
    return ((cell2 != last_cell) && ((visit && !visited_cell(cell2)) || (!visit && cell2.visited && (cell2.keys == cell1.keys))));
  }

  private void lvisit_cell(MazeCell last_cell, MazeCell cell1, boolean visit) {
   MazeCell cell2;
    if (cell1.x > 0) {
      cell2 = Map[cell1.y][cell1.x-1];
      if (valid_cell(last_cell,cell1,cell2,visit)) {
        if (visit) {
          lock_door(LEFT,RIGHT, cell1, cell2);
          visit_cell(cell1, cell2);
        }
        mazeDir = LEFT;
        cell1.gap |= LEFT;
        cell2.gap |= RIGHT;
      }
    }
  }
  
  private void tvisit_cell(MazeCell last_cell, MazeCell cell1, boolean visit) {
   MazeCell cell2;
    if (cell1.y > 0) {
      cell2 = Map[cell1.y-1][cell1.x];
      if (valid_cell(last_cell,cell1,cell2,visit)) {
        if (visit) {
          lock_door(UP,DOWN, cell1, cell2);
          visit_cell(cell1, cell2);
        }
        mazeDir = UP;
        cell1.gap |= UP;
        cell2.gap |= DOWN;
      }
    }
  }

  private void rvisit_cell(MazeCell last_cell, MazeCell cell1, boolean visit) {
   MazeCell cell2;
    if (cell1.x < XMAX-1) {
      cell2 = Map[cell1.y][cell1.x+1];
      if (valid_cell(last_cell,cell1,cell2,visit)) {
        if (visit) {
          lock_door(RIGHT,LEFT, cell1, cell2);
          visit_cell(cell1, cell2);
        }
        mazeDir = RIGHT;
        cell1.gap |= RIGHT;
        cell2.gap |= LEFT;
      }
    }
  }

  private void bvisit_cell(MazeCell last_cell, MazeCell cell1, boolean visit) {
   MazeCell cell2;
    if (cell1.y < YMAX-1) {
      cell2 = Map[cell1.y+1][cell1.x];
      if (valid_cell(last_cell,cell1,cell2,visit)) {
        if (visit) {
          lock_door(DOWN,UP, cell1, cell2);
          visit_cell(cell1, cell2);
        }
        mazeDir = DOWN;
        cell1.gap |= DOWN;
        cell2.gap |= UP;
      }
    }
  }

  //global procedure draw_maze(atom bmp, atom tset, sequence maze, sequence rect)
  public void draw(Graphics gfx, Sprite tset, int x, int y, int debug) {
   int th,x1,y1;
   Random random = new Random();
    // Ensure that each room of a maze can be exactly reproduced regardless of what tileset is used
    // by resetting the random generator to a relative constant (SEED_VALUE+th).
    // The constant does not need to be unique, just predictable.
    th = (int) tset.getRawFrameCount()/tw;
    random.setSeed(SEED_VALUE+th);
    // iterate the tiles drawing the walls
    for (int iy=0; iy < YMAX; iy++) {
      for (int ix=0; ix < XMAX; ix++) {
        x1 = x + ix*tset.getWidth();
        y1 = y + iy*tset.getHeight();
        if (Map[iy][ix].visited && (Map[iy][ix].visible || (debug != 0))) {
          tset.setFrame(tw*random.nextInt(th)+Map[iy][ix].framex);
          tset.setPosition(x1,y1);
          tset.paint(gfx);
        }
      }
    }
  }

  private static int getLockDir(MazeCell cell) { return cell.lock & 0xF; }
  public static int getLockDir(MazeCell cell, int dir) { return (cell.lock & dir); }
  private static int getKey(MazeCell cell, int index) { return (cell.lock >> 4*index) & 0xF; }
  public static int getLockKey(MazeCell cell, int dir) { return getKey(cell, 2+(dir >> 1)); }
}
