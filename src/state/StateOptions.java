/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package state;

import javax.microedition.lcdui.Graphics;
import main.Engine;
import game.GameAction;
import game.GameClass;
import game.GameState;

/**
 *
 * @author Chris
 */
public final class StateOptions extends GameState {
 private final GameAction aSELECT = new GameAction(GameClass.SOFT_RIGHT);
 private final GameAction[] ACTION_OPTIONS = new GameAction[] {aSELECT};

 private final Object mlSOUND = "Enable Sounds";
 private final Object mlMUSIC = "Enable Music";
 private final Object[] MenuList = {mlSOUND,mlMUSIC};
 private boolean[] OptList;
 private int selected = 0;

  public StateOptions(Engine engine) {
    super(engine);
    OptList = new boolean[] {engine.Sound_Enabled,engine.Music_Enabled};
  }

  protected void start() {
    OptList[0] = engine.Sound_Enabled;
    OptList[1] = engine.Music_Enabled;
    engine.SetAction(ACTION_OPTIONS);
    engine.gfx.setColor(0xFFFFFF);
  }

  protected void stop() {
    engine.RemoveAction(ACTION_OPTIONS);
  }

  protected void stateAction() {
    if (Engine.keyUp(engine.aUP)) {
      selected--;
      if (selected < 0) selected += MenuList.length;
    } else if (Engine.keyUp(engine.aDOWN)) {
      selected++;
      if (selected >= MenuList.length) selected -= MenuList.length;
    } else if (Engine.keyUp(engine.aFIRE) || Engine.keyUp(engine.aLEFT) || Engine.keyUp(engine.aRIGHT) || Engine.keyUp(aSELECT)) {
      if (selected == 0) {
        OptList[selected] = (OptList[selected] != true);
        engine.Sound_Enabled = OptList[0];
      } else if (selected == 1) {
        OptList[selected] = (OptList[selected] != true);
        engine.Music_Enabled = OptList[1];
      }
      try {
        if (engine.midi != null) {
          if (engine.Sound_Enabled && engine.Music_Enabled) {
            engine.midi.start();
          } else {
            engine.midi.stop();
          }
        }
      } catch(Exception e) {
        engine.exit();
        return;
      }
    } else if (Engine.keyUp(engine.aMENU)) { // aMENU released
      engine.SetState(engine.STATE_MENU);
    }
  }

  protected void stateUpdate() {
   int th = engine.font3.textHeight();
    engine.clearCanvas(0x000000);
    engine.font3.drawString(engine.gfx, "OPTIONS", engine.getWidth()/2, 1, Graphics.HCENTER|Graphics.TOP);
    for (int i=0; i < MenuList.length; i++) {
      if (OptList[i]) {
        engine.gfx.fillRect(32-th, 24+i*(th+4), th+1, th+1);
      } else {
        engine.gfx.drawRect(32-th, 24+i*(th+4), th, th);
      }
      engine.font3.drawString(engine.gfx, (String) MenuList[i], 36, 24+i*(th+4), Graphics.LEFT|Graphics.TOP);
      if (selected == i) {
        engine.gfx.drawRect(32-th-2, 24+i*(th+4)-2, engine.getWidth()-(32-th-2)*2, th+4);
      }
    }
    engine.drawSoftButtons(engine.font1,"BACK","SELECT");
    engine.flushGraphics();
  }
}
